# -*- coding: utf-8 -*-
import sys, re, os
from tokenizer import tokenize
import tsv
import levenstein
import letter_to_number

if len(sys.argv) < 2:
    print >>sys.stderr, 'usage: cat <synopses> | %s <tsv+>' % sys.argv[0]
    sys.exit(1)

# remove accents from text, normalize +
def unidecode(text):
    text = text.decode('utf8')
    text = re.sub(u'[éÉèÈêÊëË]', 'e', text)
    text = re.sub(u'[ïÏîÎìÌ]', 'i', text)
    text = re.sub(u'[öÖôÔòÒ]', 'o', text)
    text = re.sub(u'[àÀäÄâÂ]', 'a', text)
    text = re.sub(u'[çÇ]', 'c', text)
    text = re.sub(u'[üÜûÛùÙ]', 'u', text)
    text = text.replace('+', '-')
    return text.encode('utf8')

# words which can match interchangeably
equivalent_words = {'sucy': 'sussis', 'bus': 'ligne', 'rer a': 'rer', 'rer b': 'rer', 'rer c': 'rer', 'cdg': 'charles-de-gaulle', 'rer': 'train', 'rer': 'ligne', 'bus': 'autobus', 'square': 'place', 'ligne': 'bus', 'cles': 'clefs', 'anthony': 'antony', 'station-la': 'sation', 'roissy': 'aeroport', 'cour de vincennes': 'cours de vincennes', 'une': 'un'}

# multi-words from the corpus in order to retokenize synopses
with open('multiword-lexicon.txt') as fp:
    multiwords = [unidecode(x.strip()).lower() for x in fp.readlines()]

topics = {}
with open('topics.csv') as fp:
    for line in fp.readlines():
        tokens = line.strip().split(';')
        topics[tokens[0]] = [x for x in tokens[1:] if x != 'NO']

from speaker_type import SpeakerType
speaker_type = SpeakerType('mapping-by-id.txt')

# to detect slots in annotated synopses
pattern = re.compile(r'<a class="instance" variable="([^"]*)" style="color:[^"]*" title="[^"]*" href="#">(.*?)<')

# split slots on conjunctions and prepositional phrases to allow more matches
def make_variable_variants(tokens):
    delimiters = 'et ou à d\' de sur'.split()
    variants = [tokens]
    start = 0
    for i, token in enumerate(tokens):
        if token.text in delimiters:
            if start < i:
                variants.append(tokens[start: i])
            start = i + 1
    if start > 0 and start < len(tokens):
        variants.append(tokens[start:])
    #print start, [x.text for x in tokens], [[x.text for x in variant] for variant in variants]
    return variants

variables = {}
seen_variables = {}

# read synopses and store all slots as (variable, text)
for line in sys.stdin:
    tokens = line.strip().split()
    if len(tokens) > 3:
        template, show, annotator = tokens[:3]
        text = ' '.join(tokens[3:])
        text = text.replace('&nbsp;', ' ').lower()
        for multiword in multiwords:
            text = text.replace(re.sub('[+\s+-]+', ' ', multiword), multiword)
        if show not in variables:
            variables[show] = []
            seen_variables[show] = set()
        for variable, text in pattern.findall(text):
            tokenized = tokenize(text)
            for variant in make_variable_variants(tokenized):
                tag = '%s=%s' % (variable, ' '.join([x.text for x in variant]))
                if tag not in seen_variables[show] and len(text.strip()) > 0:
                    variables[show].append((variable, variant))
                    seen_variables[show].add(tag)

# match words using pseudo-levenstein, equivalences and normalization
def approx_word_match(orig_word1, orig_word2):
    word1 = re.sub('[+\s-]+', ' ', unidecode(orig_word1).lower())
    word2 = re.sub('[+\s-]+', ' ', unidecode(orig_word2).lower())
    if word1 == word2:
        return True
    if word2 in equivalent_words and equivalent_words[word2] == word1:
        return True
    if word1 in equivalent_words and equivalent_words[word1] == word2:
        return True
    if len(word1) > 4 and len(word2) > 4 and len(set(word1) - set(word2)) < 4:
        variants1 = set([word1[:i] + word1[i + 1:] for i in range(1, len(word1))])
        variants2 = set([word2[:i] + word2[i + 1:] for i in range(1, len(word2))])
        #print word1, word2, variants1, variants2
        if len(variants1 & variants2) > 0:
            print >>sys.stderr, 'WARNING: approx match "%s" <> "%s"' % (orig_word1, orig_word2)
            return True
    ### TEST ###
    if (len(word1) >= 9 and len(word2) >= 6 and word2 in word1) or (len(word2) >= 9 and len(word1) >= 6 and word1 in word2):
        return True
    ############
    return False

# faster match for two sequences of same length
def exact_match(sequence1, sequence2):
    if len(sequence1) != len(sequence2):
        return False
    for j in range(len(sequence2)):
        if not approx_word_match(sequence1[j].text, sequence2[j].text):
            #print 'DEBUG', sequence1[j].text, sequence2[j].text
            return False
    return (0, len(sequence1))

# check if a string represents a number
numbers = '(zero|une|un|deux|trois|quatre|cinq|six|sept|huit|neuf|dix|onze|douze|treize|quatorze|quinze|seize|((vingt|vingts|trente|quarante|cinquante|soixante)( et un| et une)?)|cent|cents|mille|mille)'
def is_number(word):
    if re.match('^[0-9]+$', word):
        return True
    if re.match('^' + numbers + '( ' + numbers +')*$', word.replace('-', ' ')):
        return True
    return False

# use letter_to_number to match numbers in words to numbers in digits
def same_number(word1, word2):
    if re.match('^' + numbers + '( ' + numbers +')*$', word1.replace('-', ' ').replace('+', ' ')):
        word1 = str(letter_to_number.convert(word1))
    if re.match('^' + numbers + '( ' + numbers +')*$', word2.replace('-', ' ').replace('+', ' ')):
        word2 = str(letter_to_number.convert(word2))
##    print '   same?', word1, word2
    return word1 == word2

# common words for which we want to reduce the mismatch cost
stopwords = set('je tu il elle ils elles nous vous avec sans pour or et ni car mais ou donc a le les la l\' un une des ce cette ces mon ma mes vos votre ton ta tes dans de par vers part de'.split())

# the cost of a substitution looks at approximations, stopwords and numbers, it gets very expensive for content words
def sub_cost(word1, word2):
#    if word2 == '21':
#        print 'NUMBER', word1, is_number(word1), word2, is_number(word2), same_number(word1, word2)
    if approx_word_match(word1, word2):
        return 0
    elif word1 in stopwords and word2 in stopwords:
        return 0.1
    elif is_number(word1) and is_number(word2) and same_number(word1, word2): 
        return 0
    return 1000

# can insert stopwords from synopses for cheap, but not content words
def ins_cost(word):
    if word in stopwords:
        return 1
    return 1000

# on the conversation, side, stopwords are cheap and content words are regular
def del_cost(word):
    if word in stopwords:
        return 0.1
    return 1

# use levenstein alignment between sequences of words
# returns False or a tuple (relative to sequence1) in case of match (alignment score < .5)
def sequence_match(sequence1, sequence2):
    num_errors, num_ref, alignment, score = levenstein.align([unidecode(x.text).lower() for x in sequence1], [unidecode(x.text).lower() for x in sequence2], 
            sub_cost=sub_cost, del_cost=del_cost, ins_cost=ins_cost)
    value = float(score) / len(sequence2)
    #print [unidecode(x.text).lower() for x in sequence1], [unidecode(x.text).lower() for x in sequence2], score, value
    #levenstein.print_alignment(alignment, sys.stderr)
    if value < .6:
        print >>sys.stderr, 'WARNING: match through alignment score=', score, num_ref
        levenstein.print_alignment(alignment, sys.stderr)

        # remove insertions and deletions at both ends of alignment
        leftmost, rightmost = levenstein.trim_alignment(alignment)
        length = 0
        start = 0
        for i in range(0, leftmost):
            if alignment[i][0] != None:
                start += 1
        for i in range(leftmost, rightmost + 1):
            if alignment[i][0] != None:
                length += 1
        #print alignment[leftmost: rightmost + 1], start, length
        return (start, length)
    return False


# find and mark occurrances of a slot (variable, words) in a sequence of tsv words
def match(sentence, variable, words):
    sentence = [x for x in sentence if x.disfluency == 'NULL']
    if len(words) == 0:
        return False
    result = False
    for i in range(len(sentence)):
        found = exact_match(sentence[i: i + len(words)], words) 
##        print 'DEBUG', found, i, [x.text for x in words], [x.text for x in sentence[i: i + len(words)]]
        if not found:
            found = sequence_match(sentence[i: i + len(words) + 2], words)
        if found:
            start, length = found
            start += i
            match_text = sentence[start].text
            if 'B-' + variable[1:] not in sentence[start].variable:
                sentence[start].variable.append('B-' + variable[1:])
            for j in range(1, length):
                match_text += ' ' + sentence[start + j].text
                if 'I-' + variable[1:] not in sentence[start + j].variable:
                    sentence[start + j].variable.append('I-' + variable[1:])
            result = match_text
    return result

num_variables = 0
num_matched = 0

def output_crf(sentences):
    for sentence in sentences:
        for word in sentence:
            if word.disfluency == 'NULL': # skip disfluencies
                parent = sentence[word.parent].lemma if word.parent >= 0 else None
                parent_pos = sentence[word.parent].postag if word.parent >= 0 else None
                slot = word.variable[0] if len(word.variable) > 0 else 'O'
                print show, word.text, word.postag, word.lemma, word.named_entity, parent, parent_pos, word.dep_label, slot
        print

def output_phrases(sentences, show):
    from collections import defaultdict
    seen = defaultdict(int)
    for sentence_num, sentence in enumerate(sentences):
        for word in sentence:
            if word.postag.lower().startswith('n'):
                label = word.variable[0][2:] if len(word.variable) > 0 and not word.has_parent(sentence, lambda x: len(x.variable) > 0) else 'O'
                parent = sentence[word.parent].lemma if word.parent >= 0 else 'ROOT'
                parent_pos = sentence[word.parent].postag if word.parent >= 0 else 'ROOT'
                features = [show, word.text, word.postag, word.lemma, word.named_entity[2:], parent, parent_pos, word.dep_label]
                # get all children that don't depend on a verb
                phrase = word.get_phrase(sentence, blocker=lambda x: x.postag.startswith('v') or x.disfluency != 'NULL') # or x.text == "jusqu'à"
                features.append(' '.join([x.text for x in phrase]))
                features.append(' '.join([x.postag for x in phrase]))
                features.append(seen['ne:' + word.named_entity[2:]])
                features.append(seen['word:' + word.lemma])
                features.append('-1' if word.parent < word.local_id else '+1')
                features.append(' '.join(topics[word.filename]) if word.filename in topics else '')
                features.append(len(phrase))
                features.append(sentence_num / len(sentences))
                features.append(speaker_type.resolve(show, word.speaker))
### Décommenté pour lancer
                print ','.join([str(x).replace(',', '<comma>') for x in features + [label]]) + '.'

                seen['ne:' + word.named_entity[2:]] += 1
                seen['word:' + word.lemma] += 1


# process all tsv passed as program argument
for filename in sys.argv[1:]:
    show = os.path.basename(filename).replace('.tsv', '')
    if show in variables:
        matched = set()
        all_variables = set()
        sentences = tsv.load(filename)
        for sentence in sentences:
            for word in sentence:
                word.variable = []
            #print '*******', ', '.join([x[0] + '=' + ' '.join([y.text for y in x[1]]) for x in variables[show]])
            for variable, words in variables[show]:
                all_variables.add(variable)
                found = match(sentence, variable, words)
                if found:
                    print >>sys.stderr, 'FOUND:', variable, found, '  |  ', ', '.join([' '.join([y.text for y in x[1]]) for x in variables[show] if x[0] == variable])
                    matched.add(variable)

        # if at least one variable matched, output conversation
        #if len(matched) > 0:
        #if len(matched) == len(all_variables):
            #output_crf(sentences)
        output_phrases(sentences, show)

        # some more debugging
        if len(matched) < len(all_variables):
            print >>sys.stderr, 'unmatched for', show
            for variable in all_variables - matched:
                print >>sys.stderr, 'UNMATCHED:', show, variable, ', '.join([' '.join([y.text for y in x[1]]) for x in variables[show] if x[0] == variable])
        num_variables += len(all_variables)
        num_matched += len(matched)

print >>sys.stderr, 'matched variables: %.2f%%' % (100 * float(num_matched) / num_variables)

