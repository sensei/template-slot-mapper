import sys

# implementation of levenstein alignment for sequences

def align(ref_words, hyp_words, sub_cost=lambda x, y: 0 if x == y else 1, ins_cost=lambda x: 0.75, del_cost=lambda x: 0.75):
    num_ref = len(ref_words)
    num_hyp = len(hyp_words)
    if num_hyp == 0:
        return num_ref, num_ref, [], []
    if num_ref == 0:
        return 0, 0, [], []
    score = []
    backtrack = []
    OK = 0
    SUB = 1
    DEL = 2
    INS = 3
    for i in range(num_ref + 1):
        score.append([0] * (num_hyp + 1))
        backtrack.append([0] * (num_hyp + 1))
        if i == 0:
            score[i][0] = 0
        else:
            score[i][0] = score[i - 1][0] + del_cost(ref_words[i - 1])
        backtrack[i][0] = DEL
    for i in range(num_hyp + 1):
        if i == 0:
            score[0][i] = i
        else:
            score[0][i] = score[0][i - 1] + ins_cost(hyp_words[i - 1])
        backtrack[0][i] = INS
    for i in range(1, num_ref + 1):
        for j in range(1, num_hyp + 1):
            sub_type = OK
            sub_value = score[i - 1][j - 1] + sub_cost(ref_words[i - 1], hyp_words[j - 1])
            del_value = score[i - 1][j] + del_cost(ref_words[i - 1])
            ins_value = score[i][j - 1] + ins_cost(hyp_words[j - 1])
            if sub_value != score[i - 1][j - 1]:
                sub_type = SUB
            if sub_value <= del_value:
                if sub_value <= ins_value:
                    score[i][j] = sub_value
                    backtrack[i][j] = sub_type
                else:
                    score[i][j] = ins_value
                    backtrack[i][j] = INS
            else:
                if del_value < ins_value:
                    score[i][j] = del_value;
                    backtrack[i][j] = DEL
                else:
                    score[i][j] = ins_value;
                    backtrack[i][j] = INS
    alignment = []
    i = num_ref
    j = num_hyp
    num_errors = 0
    while i > 0 or j > 0:
        if backtrack[i][j] == OK:
            alignment.insert(0, [ref_words[i - 1], hyp_words[j - 1]])
            i = i - 1
            j = j - 1
        elif backtrack[i][j] == SUB:
            num_errors += 1
            alignment.insert(0, [ref_words[i - 1], hyp_words[j - 1]])
            i = i - 1
            j = j - 1
        elif backtrack[i][j] == INS:
            num_errors += 1
            alignment.insert(0, [None, hyp_words[j - 1]])
            j = j - 1
        elif backtrack[i][j] == DEL:
            num_errors += 1
            alignment.insert(0, [ref_words[i - 1], None])
            i = i - 1

    return num_errors, num_ref, alignment, score[-1][-1]

def trim_alignment(alignment):
    '''remove insertions and deletions at start/end of sequence'''
    leftmost = 0
    for i in range(len(alignment)):
        if alignment[i][0] != None and alignment[i][1] != None:
            leftmost = i
            break
    rightmost = len(alignment)
    for i in reversed(range(len(alignment))):
        if alignment[i][0] != None and alignment[i][1] != None:
            rightmost = i
            break
    return leftmost, rightmost

def print_alignment(alignment, stream=sys.stdout):
    ref = []
    hyp = []
    for pair in alignment:
        if pair[0] == None:
            ref.append('*' * len(pair[1]))
            hyp.append(pair[1])
        elif pair[1] == None:
            ref.append(pair[0])
            hyp.append('*' * len(pair[0]))
        else:
            if len(pair[0]) > len(pair[1]):
                ref.append(pair[0])
                hyp.append(pair[1] + ' ' * (len(pair[0]) - len(pair[1])))
            else:
                ref.append(pair[0] + ' ' * (len(pair[1]) - len(pair[0])))
                hyp.append(pair[1])
    stream.write(' '.join(ref) + '\n')
    stream.write(' '.join(hyp) + '\n')

def wer(ref, hyp):
    num_errors, num_ref, alignment, score = align(ref, hyp)
    return num_errors

if __name__ == '__main__':
    ref = "hello"
    hyp = "hollow"
    num_errors, num_ref, alignment, score = align(ref, hyp)
    print_alignment(alignment)
    print "error_rate:", float(num_errors) / num_ref
