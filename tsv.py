# The tsv format was defined for storing Decoda annotations, each word has a few fields

#<filename> <global-wordnum> <wordnum-in-sentence> <word> NULL <postag> NULL NULL <dependency-label> <governor-wordnum> <text-id> <lemma> <morphology> <speaker> 0.0 0.0 0.0 _ <mention> <features> <corefence-label>

class Word:
    def __init__(self, tokens):
        self.filename = tokens[0]
        self.global_id = tokens[1]
        self.local_id = int(tokens[2]) - 1
        self.text = tokens[3]
        self.disfluency = tokens[4]
        self.postag = tokens[5]
        self.named_entity = tokens[6]
        self.dep_label = tokens[8]
        self.parent = int(tokens[9]) - 1
        self.text_id = tokens[10]
        self.lemma = tokens[11]
        self.morphology = tokens[12]
        self.speaker = tokens[13]
        self.mention = tokens[18] if len(tokens) > 18 else None
        self.coref_features = tokens[19] if len(tokens) > 19 else None
        self.coref_label = tokens[20] if len(tokens) > 20 else None

    def has_parent(self, sentence, criterion=lambda x: True, blocker=lambda x: False):
        node = self.parent
        visited = set()
        while node >= 0 and node < len(sentence) and node not in visited and not criterion(sentence[node]) and not blocker(sentence[node]):
            visited.add(node)
            node = sentence[node].parent
        if node < 0 or node >= len(sentence):
            return False
        if blocker(sentence[node]):
            return False
        return criterion(sentence[node])

    def get_phrase(self, sentence, blocker=lambda x: False):
        output = []
        for word in sentence:
            if word.has_parent(sentence, lambda x: x == self, blocker):
                if not blocker(word):
                    output.append(word)
        output = sorted(output + [self], key=lambda x: x.local_id)
        return output

# load a Tsv file and return an array of sentences containing each an array of words
def load(filename):
    sentences = []
    sentence = []
    with open(filename) as fp:
        for line in fp:
            line = line.strip()
            if line == '':
                if len(sentence) > 0:
                    sentences.append(sentence)
                sentence = []
            else:
                word = Word(line.split('\t'))
                sentence.append(word)
    return sentences

if __name__ == '__main__':
    import sys
    sentences = load(sys.argv[1])
    for sentence in sentences:
        for word in sentence:
            print word.text, [x.text for x in word.get_phrase(sentence)]

