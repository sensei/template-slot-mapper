import sys

if len(sys.argv) != 3:
    print >>sys.stderr, 'usage: paste <icsiboost-predictions> <icsiboost-features> | %s <icsiboost-names-file> <decision-threshold>' % sys.argv[0]
    sys.exit(1)

decision_threshold = float(sys.argv[2])

names = open(sys.argv[1]).readlines()
labels = [x.strip() for x in names[0][:-2].split(',')]
columns = {value: key for key, value in enumerate([x.split(':')[0] for x in names[1:]])}

by_show = {}

for line in sys.stdin:
    predictions, features = line.strip().split('\t')
    predictions = [float(x) for x in predictions.split()[len(labels):]]
    features = [x.strip() for x in features.split(',')]
    show = features[columns['show']]
    if show not in by_show:
        by_show[show] = [list() for x in labels]
    for i, label in enumerate(labels):
        if predictions[i] > decision_threshold:
            by_show[show][i].append(('$' + label.upper(), predictions[i], features[columns['children']]))

for show, slots in by_show.items():
    for i, slot in enumerate(slots[1:]):
        if len(slot) > 0:
            slot = sorted(slot, key=lambda x: -x[1])
            print '\t'.join([show, '$' + labels[i + 1].upper()] + ['%f %s' % x[1:] for x in slot[:10]])
