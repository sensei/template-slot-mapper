# -*- coding: utf-8 -*-

import sys, re
# dirty implementation of letters to number conversion for French

factors = {
        'mille': 1000,
        'cent': 100,
        'cents': 100,
        'soixante': 60,
        'cinquante': 50,
        'quarante': 40,
        'trente': 30,
        'vingt': 20,
        'vingts': 20,
        'seize': 16,
        'quinze': 15,
        'quatorze': 14,
        'treize': 13,
        'douze': 12,
        'onze': 11,
        'dix': 10,
        'neuf': 9,
        'huit': 8,
        'sept': 7,
        'six': 6,
        'cinq': 5,
        'quatre': 4,
        'trois': 3,
        'deux': 2,
        'un': 1,
        'une': 1,
        }

unit = {
        'mille': 5,
        'cent': 4,
        'cents': 4,
        'soixante': 3,
        'cinquante': 3,
        'quarante': 3,
        'trente': 3,
        'vingt': 2,
        'vingts': 2,
        'seize': 1,
        'quinze': 1,
        'quatorze': 1,
        'treize': 1,
        'douze': 1,
        'onze': 1,
        'dix': 1,
        'neuf': 0,
        'huit': 0,
        'sept': 0,
        'six': 0,
        'cinq': 0,
        'quatre': 0,
        'trois': 0,
        'deux': 0,
        'un': 0,
        'une': 0,
}

after = {
        'mille': '01234',
        'cent': '0123',
        'cents': '0123',
        'soixante': '015',
        'cinquante': '015',
        'quarante': '015',
        'trente': '01',
        'vingt': '015',
        'vingts': '015',
        'seize': '45',
        'quinze': '45',
        'quatorze': '45',
        'treize': '45',
        'douze': '45',
        'onze': '45',
        'dix': '1',
        'neuf': '45',
        'huit': '45',
        'sept': '45',
        'six': '45',
        'cinq': '45',
        'quatre': '245',
        'trois': '45',
        'deux': '45',
        'un': '',
        'une': '',
        }

def convert(text):
    words = re.split(r'[\s+-]+', text)
    numbers = [factors[x] for x in words if x in factors]
    units = [unit[x] for x in words if x in factors]

    for i in range(1, len(numbers)):
        if units[i] == 4 and units[i - 1] == 0:
            units[i - 1] = -1
            numbers[i] *= numbers[i - 1]
            numbers[i - 1] = 0
        elif units[i] == 2 and numbers[i - 1] == 4:
            units[i - 1] = -1
            numbers[i] *= numbers[i - 1]
            numbers[i - 1] = 0
    for i in range(len(numbers)):
        if numbers[i] == 1000:
            j = i
            while j > 0 and units[j - 1] < 5:
                j -= 1
            if j < i:
                numbers[i] *= sum(numbers[j:i])
                numbers[j:i] = [0 for x in range(i - j)]
                units[j:i] = [-1 for x in range(i - j)]
    return sum(numbers)

if __name__ == '__main__':
    for line in sys.stdin:
        line = line.strip()
        print convert(line), line

