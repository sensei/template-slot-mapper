#!/bin/bash
cat annotated-templates/*.annot | python2 map_slots_to_conversations.py tsv/*.tsv > experiments/all_data.txt

grep -v -f experiments/cccs-test-set.lst experiments/all_data.txt > experiments/icsiboost.data
grep -f experiments/cccs-test-set.lst experiments/all_data.txt > experiments/icsiboost.test

export PATH=$PATH:$HOME/work/icsiboost/icsiboost/src/
icsiboost -S experiments/icsiboost -n 100 --cutoff 5 -W 3 
icsiboost -S experiments/icsiboost -W 3 -C --posteriors < experiments/icsiboost.test \
    | paste - experiments/icsiboost.test \
    | python2 extract_best_slots.py experiments/icsiboost.names 0.1 \
    > experiments/icsiboost.test.out

echo "=== predictions in experiments/icsiboost.test.out (showing 10 lines) ==="
head experiments/icsiboost.test.out

exit

# this is for crf only (need to activate crf feature extraction in map_slots_to_conversations.py

grep -v -f experiments/cccs-test-set.lst experiments/all_data.txt > experiments/crf_train.txt
grep -f experiments/cccs-test-set.lst experiments/all_data.txt > experiments/crf_test.txt

crf_learn -f 3 -c 1 experiments/template.crf++ experiments/crf_train.txt experiments/model.crf++
crf_test -m experiments/model.crf++ < experiments/crf_test.txt | ./conlleval.pl -d '\t'

